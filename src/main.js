import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'

import pageHome from './pageHome.vue'
import pageNews from './pageNews.vue'
import pageInfo from './pageInfo.vue'

//注册组件
import divuePagination from './divuePagination.vue'
Vue.component('divue-pagination', divuePagination)

//路由配置

Vue.use(VueRouter); 

var routes = [
  { path: '/', component: pageHome},
  { path: '/pageNews', component: pageNews},
  { path: '/pageInfo', component: pageInfo}
]
var router = new VueRouter({
  routes: routes // （缩写）相当于 routes: routes

})

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
