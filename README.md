基于vue2.x分页组件  
如何自己编写一个分页组件：[https://my.oschina.net/tbd/blog/1553986](https://my.oschina.net/tbd/blog/1553986)

调用方式：  

```
<divue-pagination
    :current="current"
    :count="count"
    :pageCount="pageCount"
    :showPageNum="showPageNum"
    :showPageCount="showPageCount"
    :totalData="totalData"
    :showTotalData="showTotalData"
    :nextPage="nextPage"
    :coping="coping"
    :showSkipPage="showSkipPage"
    @runAjax="runAjax">
</divue-pagination>
```                

属性描述：  

```
        current:1,               //存放返回当前第几页
        count:2,                          //当前页前后页数    (默认：2)
        pageCount:"",            //存放返回总页数
        showPageNum:1,                    //是否显示页码     （默认值：1-显示 其他-不显示）
        showPageCount:1,               //是否显示总页数 （默认值：1-显示 其他-不显示）
        totalData:"",            //存放返回数据总条数
        showTotalData:1,               //是否显示数据总条数（默认值：1-显示 其他-不显示）
        nextPage:false,                   //是否有下一页
        coping:1,                       //是否开启首页尾页（默认值：1-显示 其他-不显示）
        showSkipPage:1,               //是否显示跳页功能（默认值：1-显示 其他-不显示
```

方法描述：
```
runAjax:function(current){//current分页组件返回的页码，从1开始
    //依据返回的页码，我们可以书写任何处理，比如发送一个ajax，成功回调返回要设置的属性，进行从新设置
}
```
